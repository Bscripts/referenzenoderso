package xyz.wa1t.ros.listener;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import xyz.wa1t.ros.EntryPoint;
import xyz.wa1t.ros.utils.User;

import java.sql.ResultSet;

public class PlayerTriggers implements Listener {

    private EntryPoint instance;

    public PlayerTriggers(EntryPoint eP) {
        this.instance = eP;
    }

    @EventHandler
    public void on(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        try {
            if(p.hasMetadata("userData")) {
                //instance.getBackendManager().removeMetaData(p, "userData");
                User user = (User)(p.getMetadata("userData").get(0).value());
                user.setLastLogin(System.currentTimeMillis());
                instance.getBackendManager().setMetaData(p, "userData", user);
                p.sendMessage("Jo, noch im Cache!");
                return;
            }
            ResultSet rs = instance.getMySQL().query("CALL playtimenetwork.get_player('" + p.getUniqueId().toString() + "')");
            if (rs.next()) {
                User user = new User();
                user.setUuid(p.getUniqueId().toString());
                user.setName(p.getName());
                user.setCoins(rs.getInt("coins"));
                user.setJoinedDate(rs.getLong("joinedDate"));
                user.setLastLogin(System.currentTimeMillis());
                instance.getBackendManager().setMetaData(p, "userData", user);
                p.sendMessage("Jo, User von MySQL!");
            } else {
                User user = new User();
                user.setUuid(p.getUniqueId().toString());
                user.setName(p.getName());
                user.setCoins(50000);
                user.setJoinedDate(System.currentTimeMillis());
                user.setLastLogin(System.currentTimeMillis());
                instance.getBackendManager().setMetaData(p, "userData", user);
                p.sendMessage("Jo, new USER!");
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @EventHandler
    public void on(PlayerQuitEvent e) {
        if(!e.getPlayer().hasMetadata("userData")) return;
        try {
            User user = (User)(e.getPlayer().getMetadata("userData").get(0).value());
            ResultSet rs = instance.getMySQL().query("CALL playtimenetwork.get_player('"+e.getPlayer().getUniqueId().toString()+"')");
            if(rs.next()) {
                instance.getMySQL().update("CALL playtimenetwork.update_player('"+e.getPlayer().getUniqueId().toString()+"', '"+user.getName()+"', '"+user.getCoins()+"', '"+user.getLastLogin()+"')");
            } else {
                instance.getMySQL().query("SELECT playtimenetwork.set_player('"+user.getName()+"', '"+e.getPlayer().getUniqueId().toString()+"', '"+user.getCoins()+"', '"+user.getJoinedDate()+"', '"+user.getLastLogin()+"')");
                System.out.println("Create new Player");
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
