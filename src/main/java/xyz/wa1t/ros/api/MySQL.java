package xyz.wa1t.ros.api;

import java.sql.*;

public class MySQL {

    public Connection con;

    public void connect(String host, String user, String password, String database) {
        try {
            con = DriverManager.getConnection("jdbc:mysql://" + host + ":3306/" + database, user, password);
            System.out.println("MySQL starting connecting...");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        if(isConnected()) {
            try {
                con.close();
                System.out.println("MySQL stopping connection...");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    public boolean isConnected() {
        return (con != null);
    }

    public void update(String query) {
        if(isConnected()) {
            try {
                PreparedStatement ps = con.prepareStatement(query);
                ps.executeUpdate();
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public ResultSet query(String query) {
        ResultSet rs = null;
        try {
            PreparedStatement ps = con.prepareStatement(query);
            rs = ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }

}
