package xyz.wa1t.ros.utils;

import com.google.gson.Gson;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import xyz.wa1t.ros.EntryPoint;

import java.io.*;

public class BackendManager {

    //region definitions
    @Getter
    private Gson gson;

    @Getter
    private String mysql_host = "";

    @Getter
    private String mysql_user = "";

    @Getter
    private String mysql_password = "";

    @Getter
    private String mysql_database = "";

    private EntryPoint instance;
    //endregion

    public BackendManager(EntryPoint eP) {
        this.instance = eP;
        gson = new Gson();
    }

    public String getConfig() {
        String result = null;
        try {
            BufferedReader br = new BufferedReader(new FileReader("plugins/referenzenoderso/config.json"));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            result = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public void loadMySQLFromConfig() {
        JSONObject object = new JSONObject(getConfig());
        mysql_host = object.getJSONObject("mysql").getString("host");
        mysql_user = object.getJSONObject("mysql").getString("user");
        mysql_password = object.getJSONObject("mysql").getString("password");
        mysql_database = object.getJSONObject("mysql").getString("database");
    }

    public void createConfig() {
        File file = new File("plugins/referenzenoderso/config.json");
        if(file.exists()) return;

        File folder = new File("plugins/referenzenoderso");
        folder.mkdir();
        JSONObject obj = new JSONObject();
        JSONObject mysql = new JSONObject();
        obj.put("created", true);

        mysql.put("host", "localhost");
        mysql.put("user", "root");
        mysql.put("password", "");
        mysql.put("database", "idk");

        obj.put("mysql", mysql);
        try (FileWriter fileWriter = new FileWriter("plugins/referenzenoderso/config.json")) {
            fileWriter.write(obj.toString());
            fileWriter.flush();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void removeMetaData(Player p, String key) {
        if(p.hasMetadata(key)) p.removeMetadata(key, instance);
    }

    public void setMetaData(Player p, String key, Object value) {
        removeMetaData(p, key);
        p.setMetadata(key, new FixedMetadataValue(instance, value));
    }

}
