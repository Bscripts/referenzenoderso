package xyz.wa1t.ros;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import xyz.wa1t.ros.api.MySQL;
import xyz.wa1t.ros.listener.PlayerTriggers;
import xyz.wa1t.ros.utils.BackendManager;

public class EntryPoint extends JavaPlugin {

    @Getter
    private EntryPoint instance;

    @Getter
    private MySQL mySQL;

    @Getter
    private BackendManager backendManager;

    @Override
    public void onEnable() {
        start();
    }

    @Override
    public void onDisable() {
        stop();
    }

    private void register() {
        backendManager = new BackendManager(this);
        instance = this;
        mySQL = new MySQL();
        Bukkit.getPluginManager().registerEvents(new PlayerTriggers(this), this);
    }

    private void start() {
        register();
        backendManager.createConfig();
        backendManager.loadMySQLFromConfig();
        mySQL.connect(backendManager.getMysql_host(), backendManager.getMysql_user(), backendManager.getMysql_password(), backendManager.getMysql_database());
    }

    private void stop() {
        mySQL.disconnect();
    }
}
